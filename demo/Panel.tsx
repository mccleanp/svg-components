import React from 'react';
import './Panel.css'

const Panel : React.FC<{}> = ({children}) => {
  return <div className="panel">{children}</div>;
}

export default Panel;