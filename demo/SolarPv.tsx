import React, { useState } from 'react';
import Panel from './Panel';
import { SolarPvElement, defineElement } from '../src/index'

defineElement('solar-pv', SolarPvElement);

export default function SolarPv() {
    const [power, setPower] = useState(3000);

    return (
        <Panel>
            <form className="controls">
            <h1>Solar PV</h1>
                <label>Power:
                    <input type="range" min={0} max={4000} value={power} onChange={e => setPower(e.target.value)} />
                </label>
            </form>
            <div className="svg">
                    <solar-pv power={power} max-power={4000}></solar-pv>
                </div>
        </Panel>
    );
};