import React, { useState } from 'react';
import Panel from './Panel';
import { OilTankElement, defineElement } from '../src/index'

defineElement('oil-tank', OilTankElement);

export default function OilTank() {
    const [temp, setTemp] = useState(60);
    const [volume, setVolume] = useState(500);

    return (
        <Panel>
            <form className="controls">
            <h1>Oil Tank</h1>
                <label>Temperature:
                    <input type="range" min={0} max={100} value={temp} onChange={e => setTemp(e.target.value)} />
                </label>
                <label>Fill Volume:
                    <input type="range" min={0} max={1000} value={volume} onChange={e => setVolume(e.target.value)} />
                </label>
            </form>
            <div className="svg">
            <oil-tank volume={volume} max-volume="1000" temperature={temp}></oil-tank>
            </div>
        </Panel>
    );
};