import React, { useState } from 'react';
import Panel from './Panel';
import { HotWaterElement, defineElement } from '../src/index'

defineElement('hot-water', HotWaterElement);

export default function HotWater() {
  const [top, setTop] = useState(60);
  const [bottom, setBottom] = useState(20);

  return (
    <Panel>
      <form className="controls">
        <h1>Hot Water</h1>
        <label>Top:
          <input type="range" min={0} max={100} value={top} onChange={e => setTop(e.target.value)} />
        </label>
        <label>Bottom:
          <input type="range" min={0} max={100} value={bottom} onChange={e => setBottom(e.target.value)} />
        </label>
      </form>
      <div className="svg">
        <hot-water top-temperature={top} bottom-temperature={bottom} style={{width: '100%', height: '100%'}}></hot-water>
      </div>
    </Panel>
  );
};