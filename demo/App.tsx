import React, { useState } from 'react';
import './App.css';
import HotWater from './HotWater';
import OilTank from './OilTank';
import SolarPv from './SolarPv';

function App() {
    const [theme, setTheme] = useState('light');

    const toggleTheme = () => {
        setTheme(theme === 'dark' ? 'light' : 'dark')
    };

    return (
        <div className={theme}>
            <div className='app'>
                <div className="toolbar"><h2>SVG web components demo</h2><button onClick={() => toggleTheme()}>Toggle theme</button></div>
                <div className="content">
                    <HotWater />
                    <OilTank />
                    <SolarPv />
                </div>
            </div>
        </div>
    );
}

export default App;