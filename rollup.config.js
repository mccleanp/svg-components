import resolve from '@rollup/plugin-node-resolve';
import hotcss from "rollup-plugin-hot-css";
import commonjs from 'rollup-plugin-commonjs-alternate';
import refresh from 'rollup-plugin-react-refresh';
import typescript from 'rollup-plugin-typescript2';
import svgo from 'rollup-plugin-svgo';
import dts from 'rollup-plugin-dts'
import { terser } from 'rollup-plugin-terser';
import brotli from "rollup-plugin-brotli";
import gzip from "rollup-plugin-gzip";
import static_files from 'rollup-plugin-static-files';
import styles from "rollup-plugin-styles";

const plugins = {
	common: [
		resolve(),
		commonjs({
			define: {
				'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
			}
		}),
		svgo({
			plugins: [
				{ cleanupIDs: false },
				{ removeViewBox: false }
			],
		}),
		typescript()
	],
	development: [
		refresh(),
		hotcss({ hot: true })
	],
	production: [
		styles({
			mode: ['extract', 'styles.css'],
			minimize: true,
			sourcemap: true
		}),
		terser(),
		gzip(),
		brotli(),
	]
};

const config = [
	{
		input: './src/index.ts',
		output: [
			{
				format: 'esm',
				dir: 'dist',
				sourcemap: true
			}
		],
		plugins: [
			...plugins['common'],
			...plugins[process.env.NODE_ENV],
		]
	},
	{
		input: "./src/index.ts",
		output: [
			{
				dir: 'dist',
				format: "esm"
			}
		],
		plugins: [typescript({useTsconfigDeclarationDir: true, tsconfigOverride: { compilerOptions: {declaration: true }}}), dts()],
	},
	{
		input: './demo/index.tsx',
		output: [
			{
				dir: 'dist/demo',
				format: 'esm',
				entryFileNames: `[name].[hash].js`,
				assetFileNames: `[name].[hash][extname]`,
				sourcemap: true,
			}
		],
		plugins: [
			...plugins['common'],
			...plugins[process.env.NODE_ENV],
			process.env.NODE_ENV === 'production' && static_files({ include: ['./demo/public'] }),
		]
	},
];

export default process.env.NODE_ENV === 'development' ? config.filter(c => c.input == './demo/index.tsx') : config;