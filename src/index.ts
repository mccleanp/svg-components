import AnnotatedSVGElement from './core/AnnotedSVGElement';

export * from './components/index';

interface NollupModule extends NodeModule {
  hot?: any | undefined;
}

declare var module: NollupModule;

export function defineElement(tag: string, clazz) {
  if (!customElements.get(tag)) {
    customElements.define(tag, clazz);
} else if (module.hot) {
  console.log("hot-reload custom element: ", tag);
  document.querySelectorAll(tag).forEach((node) => {
      const e = node as AnnotatedSVGElement;
      Object.setPrototypeOf(node, clazz.prototype);
      e.connectedCallback();
  });
}
}
