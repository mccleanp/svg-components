const hexRegex = /^#([A-Fa-f0-9]{2})([A-Fa-f0-9]{2})([A-Fa-f0-9]{2})([A-Fa-f0-9]{2})?$/;

export function hexToRGBA(hex: string) {
  if (!hex) {
    return [0,0,0,0];
  }
  const match = hex.match(hexRegex);
  if (!match) {
    return [0,0,0,0];
  }
  return match.slice(1,4)
    .map(hex => parseInt(hex, 16))
    .concat(parseInt((match[4] || 'ff'), 16));
}

interface Stop {
  value: number;
  color: number[];
}

export default function gradient() {
  const stops : Stop[] = [];

  const toHex = (value: number) => {
    const s = (~~value).toString(16);
    return s.length < 2 ? "0"+s : s;
  }

  const toColor = ( value:number, preStop: Stop, postStop: Stop) => {
    const ratio = (value - preStop.value) / (postStop.value - preStop.value);
  
    const rgba = [];
    for(let i=0; i<4; i++) {
      rgba[i] = (postStop.color[i] - preStop.color[i]) * ratio + preStop.color[i];
    }

    const color = `#${rgba.map(toHex).join('')}`;
    return color;
  }

  const findStops = (value: number) => {
    let preStop = { value: Number.MIN_SAFE_INTEGER, color: [0, 0, 0, 0] };
    let postStop = { value: Number.MAX_SAFE_INTEGER, color: [0, 0, 0, 0] };
    stops.forEach(stop => {
      if (stop.value > preStop.value && stop.value < value) {
        preStop = stop;
      }
      if (stop.value < postStop.value && stop.value >= value) {
        postStop = stop;
      }
    });
    return [preStop, postStop];
  }

  const gradient = {
    addStop: (value: number, color: string) => {
      stops.push({value, color: hexToRGBA(color)});
      return gradient;
    },
    color: (value: number) => {
      const [preStop, postStop] = findStops(value);
      return toColor(value, preStop, postStop);
    }
  };

  return gradient;
}