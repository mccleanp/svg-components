const styles = `
:host {
  display: block;
  width: 100%;
  height: 100%;
}
div {
  position: relative;
  width: 100%;
  height: 100%;
  min-height: 200px;
  min-width: 200px;
}
svg {
  position: absolute;
  width: 100%;
  height: 100%;
  fill: currentColor
}`

export default class AnnotatedSVGElement extends HTMLElement {

  annotations = [];

  constructor(svg: string) {
    super();

    this.attachShadow({ mode: 'open' });

    const template = document.createElement('template');
    template.style.display = "block";
    template.innerHTML = `<style>${styles}</style><div>${svg}</div>`;
    this.shadowRoot && this.shadowRoot.appendChild(template.content.cloneNode(true));
  }

  getAttributeFloat(name) {
    return parseFloat(this.getAttribute(name));
  }

  connectedCallback() {
    this.annotations.forEach(item => {
      item.element = this.shadowRoot.querySelector(item.selector);
    });
    this.performAnnotations();
  }

  attributeChangedCallback(name, oldValue, newValue) {
    this.performAnnotations();
  }

  annotate(selector, func) {
    this.annotations.push({selector, func});
  }

  performAnnotations() {
    this.annotations.forEach(({element, func}) => {
      if (element && func) {
        func(element);
      }
    })
  }
}