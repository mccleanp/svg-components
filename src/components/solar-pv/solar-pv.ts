import AnnotatedSVGElement from '../../core/AnnotedSVGElement';
import svg from './solar-pv.svg'

export default class SolarPvElement extends AnnotatedSVGElement {

  static get observedAttributes() {
    return ['max-power', 'power'];
  }

  constructor() {
    super(svg);
    this.annotate('#rectPower', e => e.setAttribute('transform', `scale(1, ${this.getFillRatio().toFixed(2)})`));
    this.annotate('#tspanPower', e => e.innerHTML = this.getAttributeFloat('power').toFixed(0));
  }

  getFillRatio() {
    return this.getAttributeFloat('power') / this.getAttributeFloat('max-power');
  }
}
