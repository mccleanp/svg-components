import AnnotatedSVGElement from '../../core/AnnotedSVGElement';
import svg from './hot-water.svg';
import gradient from '../../utils/gradient';

const grad = gradient()
  .addStop(20, '#3B82F6')
  .addStop(60, '#EF4444');

const TOP_TEMPERATURE = 'top-temperature';
const BOTTOM_TEMPERATURE = 'bottom-temperature';

export default class HotWaterElement extends AnnotatedSVGElement {

  static get observedAttributes() {
    return [TOP_TEMPERATURE, BOTTOM_TEMPERATURE];
  }

  constructor() {
    super(svg);
    this.annotate('#tspanTopTemp', e => e.innerHTML = this.getAttributeFloat(TOP_TEMPERATURE).toFixed(1));
    this.annotate('#tspanBottomTemp', e => e.innerHTML = this.getAttributeFloat(BOTTOM_TEMPERATURE).toFixed(1));
    this.annotate('#stopTankTop', e => e.style.stopColor = grad.color(this.getAttributeFloat(TOP_TEMPERATURE)));
    this.annotate('#stopTankBottom', e => e.style.stopColor = grad.color(this.getAttributeFloat(BOTTOM_TEMPERATURE)));
  }
}