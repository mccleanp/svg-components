import AnnotatedSVGElement from '../../core/AnnotedSVGElement';
import svg from './oil-tank.svg';

const green = '#10B981';
const amber = '#FBBF24';
const red = '#EF4444';


export default class OilTankElement extends AnnotatedSVGElement {

  static get observedAttributes() {
    return ['max-volume', 'volume', 'temperature'];
  }

  constructor() {
    super(svg);

    this.annotate('#rectVolume', e => e.setAttribute('transform', `scale(1, -${this.getFillRatio().toFixed(2)})`));
    this.annotate('#rectVolume', e => e.style.fill = this.getFillRatio() > 0.5 ? green : this.getFillRatio() > 0.2 ? amber : red);
    this.annotate('#tspanTemperature', e => e.innerHTML = this.getAttributeFloat('temperature').toFixed(1));
    this.annotate('#tspanVolume', e => e.innerHTML = this.getAttributeFloat('volume').toFixed(0));
  }

  getFillRatio() {
    return this.getAttributeFloat('volume') / this.getAttributeFloat('max-volume');
  }
}
